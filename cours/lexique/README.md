# Lexique

- [Lexique](#lexique)
  - [Protocoles](#protocoles)
    - [Couche 2 modèle OSI](#couche-2-modèle-osi)
      - [Ethernet](#ethernet)
    - [Couche 2/3 du modèle OSI](#couche-23-du-modèle-osi)
      - [ARP Adress Resolution Protocol](#arp-adress-resolution-protocol)
    - [Couche 3 du modèle OSI](#couche-3-du-modèle-osi)
      - [IP Internet Protocol v4](#ip-internet-protocol-v4)
    - [Couche 4 du modèle OSI](#couche-4-du-modèle-osi)
      - [TCP Transmission Control Protocol](#tcp-transmission-control-protocol)
      - [UDP User Datagram Protocol](#udp-user-datagram-protocol)
    - [Au dessus de 4 (couches "applicatives" qui transportent de la donnée)](#au-dessus-de-4-couches-applicatives-qui-transportent-de-la-donnée)
      - [DHCP Dynamic Host Configuration Protocol](#dhcp-dynamic-host-configuration-protocol)
      - [DNS Domain Name System](#dns-domain-name-system)
      - [HTTP HyperText Transfer Protocol](#http-hypertext-transfer-protocol)
      - [SSH Secure SHell](#ssh-secure-shell)
  - [Sigles/Acronymes](#siglesacronymes)
    - [CIDR Classless Inter-Domain Routing](#cidr-classless-inter-domain-routing)
    - [FQDN Fully Qualified Domain Name](#fqdn-fully-qualified-domain-name)
    - [LAN Local Area Network](#lan-local-area-network)
    - [MAC Media Access Control](#mac-media-access-control)
    - [RFC Request For Comments](#rfc-request-for-comments)
    - [WAN Wide Area Network](#wan-wide-area-network)
  - [Notions](#notions)
    - [Adresse de réseau](#adresse-de-réseau)
    - [Adresse de diffusion (ou *broadcast address*)](#adresse-de-diffusion-ou-broadcast-address)
      - [Pour le protocole Ethernet](#pour-le-protocole-ethernet)
      - [Pour le protocole IP](#pour-le-protocole-ip)
    - [Binaire](#binaire)
    - [Carte réseau (ou interface réseau)](#carte-réseau-ou-interface-réseau)
    - [Loopback](#loopback)
    - [Masque de sous-réseau](#masque-de-sous-réseau)
    - [Pare-feu ou *firewall*](#pare-feu-ou-firewall)
    - [Passerelle ou *Gateway*](#passerelle-ou-gateway)
    - [Ports](#ports)
    - [Routeur](#routeur)
    - [*Stack réseau* ou *stack TCP/IP* ou Pile réseau](#stack-réseau-ou-stack-tcpip-ou-pile-réseau)
    - [Subnetting](#subnetting)

## Protocoles

### Couche 2 modèle OSI

#### Ethernet

- un *message Ethernet* est une **trame Ethernet**
- utilisé pour la *commutation de paquets*
- Ethernet définit le format des trames Ethernet qui circulent sur le réseau (entre autres)
  - [MAC](#mac-media-access-control) source d'un message
  - [MAC](#mac-media-access-control) destination d'un message

- **un câble RJ45**, c'est juste un câble qui est fait pour faire passer des trames Ethernet :)
  - d'ailleurs on les appelle parfois "câbles Ethernet" !
  - **donc pas besoin de plus que d'un câble pour créer un réseau**

--

### Couche 2/3 du modèle OSI

#### ARP Adress Resolution Protocol

- c'est dans le nom : il permet de résoudre des adresses
- plus précisément, il **permet de demander sur le réseau la MAC de quelqu'un, quand on connaît son IP**

--

### Couche 3 du modèle OSI

#### IP Internet Protocol v4

> on parle ici d'IPv4 (on verra peut-être un peu d'IPv6 ensemble)

- un *message IP* est un **paquet IP**
- protocole utilisé pour discuter à travers des réseaux
- une *adresse IP* peut être "portée" par une *carte réseau*
- une *adresse IP* est composée de 32 bits
  - par exemple : `192.168.1.1`
- pour comprendre l'IP on a besoin du [masque de sous-réseau](#masque-de-sous-r%C3%A9seau) qui lui est associé

--

### Couche 4 du modèle OSI

#### TCP Transmission Control Protocol

- un *message TCP* est un **datagramme TCP**
- permet d'établir un tunnel entre deux personnes, généralement un client et un serveur
- une fois le tunnel établi, le client et le serveur peuvent échanger des données
- voyez TCP comme un échange de messages (comme des textos) **avec accusé de réception**
- **on utilise TCP lorsqu'on veut une connexion stable, même si elle est un peu plus lente**
- une connexion HTTP utilise un tunnel TCP par exemple

#### UDP User Datagram Protocol

- un *message UDP* est un **datagramme UDP**
- permet d'échanger des données, générélament entre un client et un serveur
- aucun tunnel n'est établi, les données sont envoyées **sans accusé de réception**
- **on utilise UDP lorsqu'on s'en fiche de perdre certains messages sur la route, afin d'optimiser la vitesse de transport**
- UDP est par exemple très utilisé dans les jeux en ligne (typiquement pour des FPS en ligne)

--

### Au dessus de 4 (couches "applicatives" qui transportent de la donnée)

#### DHCP Dynamic Host Configuration Protocol

- permet d'éviter aux gens de définir leur adresse IP à la main eux-mêmes
- permet donc d'attribuer automatiquement des adresses IPs au sein d'un [LAN](#lan-local-area-network)
- il existe sur les réseaux pourvus de DHCP, un *serveur DHCP*
  - chez vous, c'est votre box
  - il parle le protocole DHCP, et vos PCs aussi
- **on oppose "l'adresse par DHCP" (ou "adressage dynamique") à "l'adressage statique"**
  - une "IP statique" ça veut dire "une IP **PAS** récupérée *via* DHCP

#### DNS Domain Name System

- protocole utilisé pour associé des *un nom d'hôte et un nom de domaine* à une adresse IP
- les *serveurs DNS* sont des serveurs à qui on peut poser des questions
  - "donne moi le nom de domaine associé à telle IP"
  - "donne moi l'IP associée à tel nom de domaine"
- des outils comme [`nslookup` ou `dig`](#nslookup-ou-dig) peuvent être utilisés pour interroger des serveurs DNS à la main

#### HTTP HyperText Transfer Protocol

- protocole utilisé pour discuter avec des serveurs web
- comme beaucoup de protocole de communicationo, HTTP fonctionne avec un principe de question/réponse
  - le client effectue une requête HTTP à un serveur web
    - = il demande une page spécifique
  - le serveur répond à la requête du clent dans une réponse HTTP
    - = il donne une page HTML + son CSS et son JS au client

#### SSH Secure SHell

- protocole/outil utilisés pour se connecter à distance sur un équipement
- on peut alors contrôler l'équipement en passant par le réseau
  - l'équipement distant doit faire tourner une application : un **serveur SSH** 
    - souvent le serveur SSH écoute sur le port TCP numéro 22
  - votre PC doit posséder un **client SSH** :
    - la commande `ssh` (simple, puissant, léger)
    - ou [Putty](https://www.putty.org/) (sur Windows, quand la commande `ssh` n'est pas dispo)

## Sigles/Acronymes

### CIDR Classless Inter-Domain Routing

- de façon simple, c'est le `/24` dans `192.168.1.1/24` par exemple
- un `/24` veut dire "24 bits à 1" et correspond donc au masque `11111111.11111111.11111111.00000000` soit `255.255.255.0`

### FQDN Fully Qualified Domain Name

- c'est le nom complet d'un hôte (= d'une machine) sur le réseau
- il est la concaténation du nom d'hôte et du domaine

### LAN Local Area Network

- réseau local
- les équipements qui s'y trouvent portent des adresses privées

### MAC Media Access Control

- on parle l'ici de *l'adresse MAC* ou *adresse physique*
- elle est *obligatoirement* portée par une *carte réseau*
- sur une carte physique, elle est gravée sur la carte (on ne peut pas la changer)
- l'*adresse MAC* est composée de 12 caractères hexadécimaux (par exemple `D4-6D-7D-00-15-3F`)
- les adresses MAC servent à envoyer des messages directs aux machines sur le même réseau que nous, elles sont utilisées par [le protocole Ethernet](#ethernet)

### RFC Request For Comments

- document texte qui définit une notion, un concept, un protocole (principalement utilisés en informatique)
- les RFCs peuvent écrit par n'importe qui et sont des documents publics
- comme sont l'indique, une RFC a pour but d'être lue et commentée
- si les gens la lisent, la commentent, la complètent, une RFC finit par contenir des choses intéressantes
- **voyez-les comme des manuels pour construire des choses, ce sont juste des plans**
  - un plan pour construire une messagerie sécurisée, par exemple !
- les protocoles que vous connaissez ont tous été définis dans des RFCs, entre autres :
  - [IP](#ip-internet-protocol) : [RFC 791](https://tools.ietf.org/html/rfc791)
  - Allocation d'adresses privées : [RFC 1918](https://tools.ietf.org/html/rfc1918)
  - [HTTP](#http-hypertext-transfer-protocol) : [RFC 2616](https://tools.ietf.org/html/rfc2616)
  - [TCP](#tcp-transmission-control-protocol) : [RFC 793](https://tools.ietf.org/html/rfc793)
  - [Ethernet](#ethernet) : [RFC 826](https://tools.ietf.org/html/rfc826), [RFC 894](https://tools.ietf.org/html/rfc894)
    - un extrait de l'intro, c'est cadeau : 

> "The world is a jungle in general, and the networking game contributes many animals."

### WAN Wide Area Network

- réseau étendu
- celui que vous utilisez le plus est Internet
  - les équipements qui s'y trouvent portent des [adresses publiques](./3.md#ip-privéespubliques)

## Notions

### Adresse de réseau

- adresse qui définit un réseau
- elle ne peut pas être utilisée comme adresse d'un hôte
- elle correspond à la première adresse disponible d'un réseau
- exemple :
  - pour l'adresse IP `192.168.1.37/24`, l'adresse de réseau est `192.168.1.0/24`

### Adresse de diffusion (ou *broadcast address*)

#### Pour le [protocole Ethernet](#ethernet)

- dernière MAC "possible" : `ff:ff:ff:ff:ff:ff`
- elle n'est portée par aucun équipement
- envoyer une trame Ethernet à cette [adresse MAC](#mac-media-access-control) permet d'envoyer un message à tous les autres équipements sur le réseau où l'on se trouve

#### Pour le [protocole IP](#ip-internet-protocol-v4)

- dernière adresse d'un réseau
- est utilisée pour envoyer un message à tous les hôtes d'un réseau
- elle ne peut pas être utilisée comme adresse d'un hôte
- **l'adresse de broadcast n'est portée par aucune machine**, c'est une adresse réservée dans tous les LANs du monde :)
- exemple :
  - pour l'adresse IP `192.168.1.37/24`, l'adresse de broadcast est `192.168.1.255/24`

### Binaire

- c'est la base 2 des mathématiques
- nous sommes habitués à compter en base 10
- n'importe quelle base est possible, certaines sont beaucoup utilisées en informatique
  - binaire (base 2)
  - décimal (base 10)
  - octal (base 8)
  - héxadécimal (base 16)
  - base 64 (oui oui, base 64)
  - pour les curieux : en fait l'ASCII normal (pas la table étendue), c'est juste une base 128 hein :)
 
### Carte réseau (ou interface réseau)

- carte physique dans une machine (ou virtuelle)
- porte forcément une [adresse MAC](#mac-media-access-control)
  - pour une interface physique, la MAC est gravée physiquement sur le périphérique : on ne peut pas la changer
- peut porter une [adresse IP](#ip-internet-protocol) et ainsi "être dans un réseau"
- dans nos PCs du quotidien, on en a au moins une : la carte WiFi

### Loopback

- quand on parle de Loopback on parle d'une interface réseau
- il existe au moins une Loopback sur tous les équipements
  - sur vos PCs elle porte l'IP `127.0.0.1`
  - parfois elle est gérée différemment par rapport aux autres interfaces (vous ne la voyez pas sur Windows avec un `ipconfig`)
- **une interface de loopback permet uniquement de se joindre soi-même**
  - en fait c'est juste qu'elle porte une IP dans un `/32`, un masque un peu particulier, utilisé à cet effet :)
- go test un `ping 127.0.0.1` vous aurez jamais vu un `ping` si rapide. Normal : vous pingez votre propre machine, sans passer par le réseau

### Masque de sous-réseau

- permet d'extraire l'[adresse de réseau](#adresse-de-réseau) d'une adresse IP
- se présente sous sa forme classique `255.255.255.0` ou en notation [CIDR](#cidr-classless-inter-domain-routing) `/24`
- **les trois affirmations suivantes sont parfaitement équivalentes :**
  - "j'ai un réseau en `255.255.255.0`"
  - "j'ai un `/24`"
  - "j'ai un réseau avec 256 adresses possibles"

### Pare-feu ou *firewall*

- présent sur la plupart des équipements (PCs, serveurs, etc)
- peut exister sous la forme d'une équipement physique
- c'est une application qui permet de filtrer le trafic réseau d'une machine
  - filtrage du trafic qui entre sur la machine
    - par exemple le firewall Windows bloque le `ping` entrant par défaut
  - filtrage du trafic qui sort de la machine
    - surtout utilisé sur des PCs d'entreprise ou sur des serveurs

### Passerelle ou *Gateway*

- la *passerelle* est un noeud agissant comme pivot, et elle permet de sortir du réseau (de chez vous vers Internet par exemple)
- il existe des réseaux sans passerelle
- la passerelle possède souvent l'IP juste avant la *broadcast*, mais pas toujours (ce n'est pas le cas à Ingésup par exemple)
- **la passerelle est une machine. L'adresse IP de gateway est donc l'adresse IP d'une machine présente sur le même réseau que nous**. C'est une adresse IP réelle, et elle est portée par un équipement.
- **un réseau qui ne souhaite pas être connecté à d'autres réseaux ne possède pas de passerelle**

### Ports

- un serveur est une machine qui "écoute" sur un port
  - un client est une machine qui se connecte à un port où un serveur écoute
- un port est un point d'entrée unique **sur une interface réseau**
  - donc si on a deux interfaces réseau, on a deux ports 443 (entre autres) : un sur chaque interface
- on peut demander à des applications "d'écouter" sur un ou plusieurs ports
- *par exemple, pour un site web, on demande souvent au serveur Web d'écouter sur le port 443 pour HTTPS*

### Routeur

- **Très important**
- un routeur est un équipement sur le réseau 
  - c'est un PC quoi, mais optimisé :)
- il est au milieu de plusieurs réseaux, au moins deux (sinon c'est pas un routeur !)
  - **pour rappel** : "être dans un réseau" = "être branché (câble ou wifi) **+** posséder une carte réseau **+** avoir une IP dans le réseau donné"
  - donc il a au moins deux interfaces réseaux
  - et donc au moins deux adresses IPs ! :)
  
```
 ____________                      ____________
|  Réseau 1  |                    |  Réseau 2  |
|            |--- Routeur ---|            |
|____________|                    |____________|
```

- **il permet aux gens du réseau 1 d'aller vers le réseau 2, et vice-versa**

- la [passerelle](#passerelle-ou-gateway) d'un réseau, c'est souvent un routeur !

### *Stack réseau* ou *stack TCP/IP* ou Pile réseau

- désigne toutes les applications d'une machine qui s'occupent du réseau
- si quand vous tapez `ipconfig` il se passe quelque chose, bah y'a bien une application qui s'en occupe
- désigne des choses bien différentes suivant les OS
- on utilise le terme *stack réseau* pour désigner tout ce qui touche de près ou de loin au réseau sur une machine
  - gestion d'IP
  - gestion d'interfaces
  - firewall
  - et plein plein d'autres choses

### Subnetting

- c'est le fait de découper un réseau en plusieurs sous-réseaux
- par exemple, un `/24` contient deux `/25`
  - `192.168.1.0/24` est la réunion de `192.168.1.0/25` et `192.168.1.128/25`
- il existe des tonnes d'outils permettant d'assister le subnetting en évitant de devenir fou avec le binaire, comme [celui-ci](http://www.davidc.net/sites/default/subnets/subnets.html)
