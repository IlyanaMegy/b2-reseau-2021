# DNS

* [I. Présentation](#i.-présentation)
  * [1. Intro](#1.-intro)
  * [2. Vocabulaire](#2.-vocabulaire)
* [II. Les serveurs DNS](#ii.-les-serveurs-dns)
  * [1. Enregistrements DNS](#1.-enregistrements-dns)
  * [2. Les fichiers de zone](#2.-les-fichiers-de-zone)
  * [3. L'interaction client serveur](#3.-l'interaction-client-serveur)
    * [Exemples](#exemples)
  * [4. Différents types de serveur DNS](#4.-différents-types-de-serveur-dns)
    * [Serveur DNS autoritatif](#serveur-dns-autoritatif)
    * [Serveur DNS forwarder](#serveur-dns-forwarder)
* [III. L'architecture DNS internet](#iii.-l'architecture-dns-internet)
  * [1. Les DNS root servers](#1.-les-dns-root-servers)
  * [2. Les serveurs DNS de premier niveau](#2.-les-serveurs-dns-de-premier-niveau)
  * [3. Les serveurs DNS de deuxième niveau](#3.-les-serveurs-dns-de-deuxième-niveau)
  * [4. Résumé](#4.-résumé)

## I. Présentation

### 1. Intro

***DNS*** signifie *Domain Name System*.

C'est un protocole qui permet de **gérer des noms d'hôte et de domaine, afin de les associer à des adresses IP.**

Ainsi il existe des ***serveur DNS*** qui connaissent :

- des associations noms ➜ IP
- des associations IP ➜ noms

**Les serveurs DNS peuvent alors être interrogés par des clients du réseau pour obtenir ces informations.**

> On peut imager les serveurs DNS comme les pages jaunes du réseau, et par extension, de l'internet : il connaissent l'adresse IP de tous les noms de domaine.

C'est grâce aux serveurs DNS qu'un navigateur Web peut connaître l'addresse IP qui correspond à un nom comme `www.ynov.com` et ainsi être en mesure de visiter le site web qui se trouve à cette adresse IP.

### 2. Vocabulaire

Dans un nom comme `www.ynov.com` on distingue trois choses :

- `www` est le *nom d'hôte* ou ***hostname***
- `ynov.com` est le *nom de domaine* ou ***domain name***
- `www.ynov.com`, l'ensemble, est appelé ***FQDN*** pour *Fully Qualified Domain Name*

## II. Les serveurs DNS

### 1. Enregistrements DNS

Un enregistrement DNS *("DNS record")* est une information atomique que peut connaître un DNS. 

| Nom de l'enregistrement | Utilité                                                              |
|-------------------------|----------------------------------------------------------------------|
| A                       | Permet de définir une correspondance entre un nom et une adresse IP  |
| PTR                     | Permet de définir une correspondance entre une adresse IP et un nom  |
| NS                      | Permet de définir l'adresse du serveur DNS qui gère un domaine donné |

**Dit plus simplement**, si on veut dire à notre DNS qu'on a ajouté une nouvelle machine dans le réseau, on va créer un nouvel *enregistrement A*, qui renseigne le nom de cette machine, et son IP.  

**Ainsi, les clients pourront résoudre le nom ajouté, et obtenir l'adresse IP associée.**

### 2. Les fichiers de zone

Un ***fichier de zone*** est un simple fichier texte. **Il contient une liste d'enregistrements DNS.**

On définit un *fichier de zone* (de chaque type) pour chaque domaine que l'on gère.

Il existe deux types de *fichiers de zone* :

- ***forward***
  - un *fichier de zone forward* contient principalement des *enregistrements A*
  - il contient donc des associations de nom vers des adresses IP
  - **c'est l'utilité principale du DNS : permettre aux client de résoudre des noms en IP**
- ***reverse***
  - un *fichier de zone reverse* contient principalement des *enregistrements de type PTR*
  - il contient donc des associations d'adresses IP vers un nom

### 3. L'interaction client serveur

Un serveur DNS est un serveur qui écoute par convention sur le port 53 en UDP.

Les clients qui connaissent l'IP du serveur et peuvent la joindre peuvent alors lui demander les informations qu'il connaît.

#### Exemples

Un exemple avec la commande `dig` :

```bash
# Demandons l'IP associée au nom `www.ynov.com` au serveur DNS de CloudFlare (1.1.1.1)
# Autrement dit, on va envoyer une requête DNS vers 1.1.1.1
# Cette requête demande l'enregistrement A associé à `www.ynov.com`
$ dig www.ynov.com @1.1.1.1
[...]
;; QUESTION SECTION:
;www.ynov.com.			IN	A

;; ANSWER SECTION:
www.ynov.com.		3600	IN	A	92.243.16.143

;; SERVER: 1.1.1.1#53(1.1.1.1)
[...]
```

La `QUESTION SECTION` c'est ce que nous on a demandé : *l'enregistrement A* associé au nom `www.ynov.com`.  
La `ANSWER SECTION` c'est la réponse du serveur DNS : on sait désormais que *l'enregistrement A* qu'on a demandé associe le nom `www.ynov.com` à l'adresse IP `92.243.16.143`.  
La section `SERVER` contient l'adresse IP du serveur DNS qui nous a répondu.

### 4. Différents types de serveur DNS

En cours, on s'est assez peu étendus sur tous les types de DNS. Pour simplifier, nous avons parlé de deux types de serveurs DNS :

- ***autoritatif***
- ***forwarder***

#### Serveur DNS autoritatif

Un ***serveur DNS autoritatif*** est un serveur DNS qui est responsable d'un domaine, d'une zone.  
**Ainsi, c'est sur ce serveur qu'on trouve des *fichiers de zone*.**

**C'est lui et lui seul qui connaît les adresses IP pour le domaine dont il est responsable.**

#### Serveur DNS forwarder

Un ***serveur DNS forwarder*** est un serveur qui ne contient aucun *fichier de zone*.

Son seul travail est de réceptionner des requêtes DNS venant de clients, afin de les relayer (de les *forward*) vers un autre serveur DNS.  
Lorsqu'il recevra la réponse, il la fera passer au client qui lui a envoyé la requête initiale.

## III. L'architecture DNS internet

Un nom de domaine comme `www.ynov.com` est constitué de plusieurs parties, est chaque partie va être utilisée de façon indépendante pour une résolution de noms complète. 

Pour expliquer une résolution de nom complète, on va utiliser l'exemple filé de `www.ynov.com.`.

> Notez la présence d'un point `.` final à la fin du nom. Il est strictement obligatoire lorsque l'on veut comprendre comment fonctionne une résolution DNS.

### 1. Les DNS root servers

Les ***serveurs DNS racine*** sont des *serveurs autoritatifs* un peu particuliers. En effet, leur rôle est de connaître l'adresse des *serveurs DNS de premier niveau*.  

Ainsi les *serveurs DNS racine* initient la résolution en commençant par lire le point `.` final.  

Pour le nom `www.ynov.com.` le rôle du serveur DNS racine sera d'indiquer à celui qui effectue la requête à quelle adresse IP se trouve le *serveur DNS autoritarif* pour la zone `.com.`.

> Lorsqu'on demande l'adresse IP d'un serveur DNS autoritatif pour une zone, on demande *l'enregistrement NS* de la zone.

### 2. Les serveurs DNS de premier niveau

Les ***serveurs DNS de premier niveau*** sont des *serveurs DNS autoritatifs* qui ont pour responsabilité les domaines en dessous le point `.` final.

Par exemple, dans le cas d'une résolution de `www.ynov.com.`, le *serveur DNS de premier niveau* aura pour rôle d'indiquer à celui qui effectue la requête l'adresse IP du serveur autoritatif de la zone `ynov.com.` (ce sera un *serveur DNS de deuxième niveau*).

### 3. Les serveurs DNS de deuxième niveau

Une fois que le client connaît l'adresse IP du *serveur DNS autoritatif* de la zone `ynov.com.` (c'est un serveur DNS de deuxième niveau), le client lui demande où est l'adresse IP du serveur `www.ynov.com.` afin de, par exemple, visiter le site web qui s'y trouve.

> Lorsqu'on demande l'adresse IP d'une machine donnée, on demande l'enregistrement A associé au nom de cette machine.

### 4. Résumé

Pour résoudre le nom `www.ynov.com.` et obtenir l'adresse IP associée, la requête suivra le chemin suivant :

- demander aux *root servers* l'enregistrement NS pour la zone `.com.`
  - on dit qu'il résout le *TLD* *(top-level domain)*
- demander aux *serveurs de premier niveau* l'enregistrement NS pour la zone `.ynov.com.`
- demander au *serveur de deuxième niveau* l'enregistrement A associé au nom `www.ynov.com.`
  - il s'agit ici du serveur DNS autoritatif d'YNOV
  - il connaît toutes les adresses IP des serveurs dans la zone `ynov.com.`

![DNS architecture](./pic/what-is-a-root-domain.png)
